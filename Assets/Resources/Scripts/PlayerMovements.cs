﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerMovements : MonoBehaviour
{
    [SerializeField] public Image editScroll;
    private CharacterController character_Controller;
    private Vector3 move_Direction;


    public float walkSpeed = 5f;
    public float runSpeed = 10f;
    private float gravity = 20f;

    public float jump_Force = 10f;
    private float vertical_Velocity;

    //-----------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------PRIVATE METHODS-------------------------------------------------------//
    //-----------------------------------------------------------------------------------------------------------------------------//
    private void Start()
    {
        character_Controller = GetComponent<CharacterController>();
    }

    private void Update()
    {
        float speed = PlayerActionChoice();
        MovePlayer(speed);
    }

    private float PlayerActionChoice()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            return runSpeed;
        }
        else
        {
            return walkSpeed;
        }
    }

    private void MovePlayer(float speed)
    {
        // Move
        move_Direction = new Vector3(Input.GetAxis("Horizontal"), 0f,
                                     Input.GetAxis("Vertical"));

        move_Direction = transform.TransformDirection(move_Direction);
        move_Direction *= speed * Time.deltaTime;

        ApplyGravity();

        character_Controller.Move(move_Direction);
    }

    private void ApplyGravity()
    {
        vertical_Velocity -= gravity * Time.deltaTime;

        // jump
        PlayerJump();

        move_Direction.y = vertical_Velocity * Time.deltaTime;
    }

    private void PlayerJump()
    {
        if (character_Controller.isGrounded && Input.GetKeyDown(KeyCode.Space))
        {
            vertical_Velocity = jump_Force;
        }
    }
}