﻿using UnityEngine;
using UnityEngine.UI;

public class CameraHandler : MonoBehaviour
{
    [SerializeField] public Image editScroll;
    // horizontal rotation speed
    private float horizontalSpeed = 10f;

    // vertical rotation speed
    private float verticalSpeed = 10f;
    private float xRotation = 0.0f;
    private float yRotation = 0.0f;

    private bool thirdPersonCamera;

    private bool isSnowActive;
    //-----------------------------------------------------------------------------------------------------------------------------//
    //-------------------------------------------------------PRIVATE METHODS-------------------------------------------------------//
    //-----------------------------------------------------------------------------------------------------------------------------//

    private void Update()
    {
        RotateCamera();
    }

    private void RotateCamera()
    {
        float mouseX = Input.GetAxis("Mouse X") * horizontalSpeed;
        float mouseY = Input.GetAxis("Mouse Y") * verticalSpeed;

        yRotation += mouseX;
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);

        transform.localEulerAngles = new Vector3(xRotation, yRotation, 0);

    }
}
